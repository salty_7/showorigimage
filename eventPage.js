var parentId = chrome.contextMenus.create({
  "id" : "showOrigImage",
  "title" : ":origな画像を新しいタブで開く",
  "type" : "normal",
  "contexts" : ["image"],
  "targetUrlPatterns" : ["*://pbs.twimg.com/media/*"],
});

chrome.contextMenus.onClicked.addListener( function(info){
  var imgUrl = info.srcUrl.replace(/name=.*$/g, "name=orig");
  var w = window.open('about:blank');
  w.document.write("<img src='" + imgUrl + "'/>");
});
